var mainState ={
preload:function() {

    this.load.tilemap('map', 'assets/map5.json', null, Phaser.Tilemap.TILED_JSON)
    this.load.image('tile1', 'assets/wall.png')
    this.load.image('tile2', 'assets/chest.png')
    this.load.image('player', 'assets/player.png')
},
create:function() {

    // 新增地圖
    this.map = this.game.add.tilemap('map')
  
    // 新增圖塊 addTilesetImage( '圖塊名稱' , 'load 時png的命名' )
    this.map.addTilesetImage('wall', 'tile1')
    this.map.addTilesetImage('chest', 'tile2')
  
    // 建立圖層 (圖層名稱為 tile 中所設定)
    this.layer = this.map.createLayer('layer')
    this.layer.resizeWorld()
    //collide
    this.map.setCollision(592);
    this.map.setCollision(668);
    this.map.setCollision(670);
    this.map.setCollision(632);
    this.map.setCollision(665);
    this.map.setCollision(672);
    this.map.setCollision(674);
    this.map.setCollision(707);
    this.map.setCollision(711);
    this.map.setCollision(713);
    this.map.setCollision(717);
    this.map.setCollision(721);
    this.map.setCollision(1186);

    
    //this.layer.debug = true//debugging
  
    // 新增物理引擎 
    this.game.physics.startSystem(Phaser.Physics.ARCADE)
    //player creat
    this.player = game.add.sprite(200, 200, 'player');
    /*this.player.scale.setTo(0.5, 0.5);*/ 
    game.physics.arcade.enable(this.player);
    this.player.body.collideWorldBounds = true
    // 鏡頭跟隨玩家
    this.game.camera.follow(this.player, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1)
    //key inputs
    this.cursor =game.input.keyboard.createCursorKeys();

},
update: function() {
    this.movePlayer();
    game.physics.arcade.collide(this.player, this.layer);
    //console.log(this.player.x)
    //console.log(this.player.y)//debugging
},
movePlayer: function() {
    this.player.body.velocity.x = 0
    this.player.body.velocity.y = 0
    if (this.cursor.left.isDown)
    this.player.body.velocity.x = -250;
    if(this.cursor.right.isDown)
    this.player.body.velocity.x = 250;
    if(this.cursor.up.isDown)
    this.player.body.velocity.y = -250;
    if(this.cursor.down.isDown)
    this.player.body.velocity.y = 250;            
}
}
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');
game.state.add('main', mainState);
game.state.start('main');
